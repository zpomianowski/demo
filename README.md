# SmartM2M demo
Mały projekt na szybko. Ma na celu wizualizować statystyki z koncentratorów (mierniki wody, prądu i gazu - w tej roli Raspberry PI).

Całość bazuje na kilku środowiskach / językach / technologiach / whatever.
Poniżej słowa klucze:

- influxdb, sqlite
- Ruby, RubyGems
- golang
- javascript, node, npm
- sass
- typescript, angular
- python, pip
- systemd

Ogólna koncepcja (na ten moment tylko grafana):
![Koncepcja](misc/concept.jpg "Koncepcja")

Testowane na:

- _Ubuntu 16.04 LTS_ - serwer
- _Raspbian Stretch Lite_ - koncentrator Raspberry PI

Autor: [Zbigniew Pomianowski](mailto:zpomianowski@cyfrowypolsat.pl)

---
## Instalacja InfluxDB i serwera reverse-proxy Nginx (Ubuntu)
```bash
sudo apt update
sudo apt install nginx
wget https://dl.influxdata.com/influxdb/releases/influxdb_1.3.6_amd64.deb && sudo dpkg -i influxdb_1.3.6_amd64.deb && rm *.deb
wget https://dl.influxdata.com/chronograf/releases/chronograf_1.3.9.0_amd64.deb && sudo dpkg -i chronograf_1.3.9.0_amd64.deb && rm *.deb
# basepath do zmiany, jeśli chronograf ma być za reverse-proxy serwerem
sudo systemctrl start influxdb
```
Dla innych platform niż _Debian_: [tutaj](https://portal.influxdata.com/downloads).

Konsola influxa, tworzymy bazę **m2m** dla projektu:
```bash
influx
> SHOW DATABASES
> CREATE DATABASE m2m
> SHOW DATABASES # powinniśmy zobaczyć nowy wpis "m2m"
> use m2m
> CREATE USER m2m WITH PASSWORD 'smartm2m' # przykład
> GRANT ALL ON m2m TO m2m
> SHOW GRANTS FOR m2m
```

Plik konfiguracyjny `/etc/influxdb/influxdb.conf`

Bezpieczeństwo DB: **TODO**, na ten moment defaultowy user _root/root_

---
## Środowisko prod - GRAFANA
Paczka wygenerowana w ramach dev (zoptymalizowana paczka).
```bash
sudo dpkg -i grafana_4.5.2-1509107521_amd64.deb
sudo systemctl start grafana-server
```

### Deployment i HTTPS - **TODO, bo brak publicznego serwera i domeny**
Upewnij się, że SSL jest w wersji _PROD_.
```bash
sudo cp scripts/m2m /etc/nginx/sites-available/
cd /etc/nginx/sites-enabled/
sudo rm default
sudo ln -s ../sites-available/m2m m2m
sudo systemctl restart nginx.service
```

Zakładam generację certufikatów via letsenrcypt:
```bash
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get -y install python-certbot-nginx
```

Jednorazowa konfiguracja:
```bash
sudo certbot --nginx
sudo certbot --nginx certonly # gdy ręcznie edytujemy congig nginx
```
Certyfikaty letsencrypt mają ważność tylko 3 miesięcy, stąd potrzeba periodycznych aktualizacji - do crona roota trzeba dodać:
```bash
# m h dom mon dow   command
  0 5 10  *   *     certbot renew
```
Taki wpis będzie przeprowadzał próbę przedłużenia ważności certyfikatu o 5:00 rano, 10. każdego miesiąca.

## Środowisko dev - GRAFANA
Potrzebny jest upgrade **go** do **1.9.X**:
```bash
sudo add-apt-repository ppa:gophers/archive
sudo apt update
sudo apt-get install golang-1.9-go
```

Sprawdzamy gdzie jest bin do **go** i przepinamy się ze starego środowiska na nowe:
```bash
sudo rm /usr/bin/go
sudo ln -s /usr/lib/go-1.9/bin/go go
```

Będąc najwyżej w strukturze folderów tego projektu:
```bash
export GOPATH=`pwd`/grafana
cd grafana/src/github.com/grafana/grafana/
go run build.go setup
go run build.go build
sudo npm install -g yarn
yarn install --pure-lockfile
npm run build
```

Podczas developerki przydaje się:
```bash
go get github.com/Unknwon/bra
bra run
# Problem: bra is not a recognized command
# Solution: export PATH=$PATH:$GOPATH/bin
```

### Build zoptymalizowanej paczki
```bash
sudo apt-get -y install ruby-all-dev
sudo gem install fpm
go run build.go build package
```

### HTTPS
Upewnij się, że SSL jest w wersji _DEV_.
```bash
sudo cp scripts/m2m /etc/nginx/sites-available/
cd /etc/nginx/sites-enabled/
sudo rm default
sudo ln -s ../sites-available/m2m m2m
sudo systemctl restart nginx.service
```

---
## Środowisko prod - Raspberry PI3
Trzy opcje:
```bash
./main.py               # odpala neverending loop
./main.py -h            # pomoc, krótkie how-to
./main.py --shell       # interaktywna powłoka, głównie do edycji db
./main.py --sim [X]     # symuluje na X dni wstecz, a potem odpala loop
                        # gdy X nie jest dane, pobierana jest wartość z pliku *.ini
./main.py --webdb       # odpala web interfejs do zarządzania bazą lokalną RPI
                        # serwis nie może być aktywny! sqlite jest jednowątkowy
./main.py --reset       # czysći bazę i historię pomiarów
                        # generuje domyślne rekordy wg ustawień w pliku *.ini
```

Tworzymy usera `m2m` dla naszego serwisu:
```bash
sudo useradd -r -s /bin/false m2m
```
Instalujemy:
```bash
sudo ./push2rpi.py -h <ip_rpi> -u <user_rpi_sudoer>
```

### Logi
RPI loguje swoje działania do plików, które są zdefiniowane w pliku `rpi/appconfig.ini` jako `logPath`. Logi są zamykane, gdy przekroczą wielkość 1MB i podlegają retencji - kolejne historyczne fragmenty zapisywane są z dodatkowymi numerami `*.log.X`. Im większy numer, tym starsze. Maksymalnie do **9**.

### USB modem
```bash
lsusb # Dla HW E398 12d1:1505
```
Bierzemy _VendorID_ i _ProductID_ modemu (posłużymy się 12d1:1505). Potem...
```bash
sudo echo 'ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="12d1", ATTRS{idProduct}=="1505", RUN+="/usr/sbin/usb_modeswitch -v 12d1 -p 1505 -J"' | sudo tee /etc/udev/rules.d/98-huawei.rules
```
Chodzi o to, że urządzenie USB może pracować jako mode, pendrive itp itd. Trzeba je odpowiednio _przełączyć_.
Co Vendor to inna historia. Magiczna opcja __-J__ jest dla Huaweia. Dla innych dostawców pewnie jest inaczej... trudniej / łatwiej?

## Środowisko dev - Raspberry PI3
Bebechy dla RPI są tutaj:
```bash
cd rpi
sudo apt update
sudo apt install virtualenv python-dev
virtualenv venv
source venv/bin/activate
pip install --extra-index-url=https://gergely.imreh.net/wheels/ numpy
pip install -r requirements/dev.txt
cp appconfig.ini.example appconfig.ini
```

Pracujemy i odpalamy sofcik w ramach izolowanego środowiska wirtualnego. Po aktywacji w okrągłych nawiasach powinna być widoczna nazwa: _(venv)_
