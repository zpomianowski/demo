#! /usr/bin/env ../rpi/venv/bin/python
# -*- coding: utf-8 -*-
import os
import re
import sys
import argparse
import paramiko
from getpass import getpass


def install():
    ignore_ext = [r'pyc$', r'table$', r'log$', r'^\.~', r'#$']
    if not args.ini:
        ignore_ext.append(r'ini$')
    if not args.db:
        ignore_ext.append(r'db$')
    ignore_dir = ['/venv']
    inc = 0
    for dirpath, dirnames, filenames in os.walk(lpath):
        if any([i in dirpath for i in ignore_dir]):
            continue

        _rpath = dirpath.replace(lpath, tpath)
        try:
            sftp.listdir(_rpath)
        except IOError:
            sftp.mkdir(_rpath)

        for filename in filenames:
            if any([re.search(i, filename) != None for i in ignore_ext]):
                continue
            sftp.put(
                os.path.join(dirpath, filename),
                os.path.join(_rpath, filename))

    first_install = True
    try:
        if 'venv' in sftp.listdir(rpath):
            first_install = False
    except IOError:
        pass

    if first_install:
        cmd = (
            'sudo apt update && sudo apt -y install '
            'virtualenv python-dev python-pip htop libmysqlclient-dev && '
            'sudo pip install --upgrade pip setuptools')
        stdin, stdout, stderr = ssh.exec_command(cmd, get_pty=True)
        for line in iter(stdout.readline, ""):
            print line.replace('\n', '')

        cmd = (
            'cd %s && virtualenv venv && ' % tpath +
            'source venv/bin/activate && '
            'pip install --upgrade pip setuptools && '
            'pip install --extra-index-url=https://gergely.imreh.net/wheels/ numpy && '
            'pip install -r requirements/prod.txt')
        stdin, stdout, stderr = ssh.exec_command(cmd, get_pty=True)
        for line in iter(stdout.readline, ""):
            print line.replace('\n', '')

    ssh.exec_command('sudo chown pi:pi -R %s' % rpath)
    ssh.exec_command('sudo rsync -r %s/ %s' % (tpath, rpath))
    ssh.exec_command('sudo chmod +x %s/main.py' % rpath)
    if first_install:
        cmd = (
            'cd %s && virtualenv venv && ' % rpath +
            'source venv/bin/activate && '
            'pip install --upgrade pip setuptools && '
            #'pip install --extra-index-url=https://gergely.imreh.net/wheels/ numpy && '
            'pip install -r requirements/prod.txt')
    else:
        cmd = (
            'cd %s && source venv/bin/activate && ' % rpath +
            'pip install -r requirements/prod.txt')
    stdin, stdout, stderr = ssh.exec_command(cmd, get_pty=True)
    for line in iter(stdout.readline, ""):
        print line.replace('\n', '')

    if first_install:
        cmd = ('sudo cp %(p)s/appconfig.ini.example %(p)s/appconfig.ini' %
            dict(p=rpath))
        ssh.exec_command(cmd)

    ssh.exec_command('sudo chown m2m:m2m -R %s' % rpath)


def uninstall():
    cmd = (
        'sudo systemctl disable m2m-rpi.service && '
        'sudo systemctl stop m2m-rpi.service && '
        'sudo chown pi:pi -R %(p)s && '
        'rm -R %(p)s && '
        'sudo rm /lib/systemd/system/m2m-rpi.service') % dict(p=rpath)
    ssh.exec_command(cmd)


def set_systemd():
    sftp.put(
        os.path.join(lpath, '../scripts', 'm2m-rpi.service.tmplt'),
        os.path.join(tpath, 'm2m-rpi.service.tmplt'))
    cmd = (
        "sed -e 's@{{INSTALL_PATH}}@%s@g' %s/m2m-rpi.service.tmplt | sudo tee "
        % (rpath, tpath) + "/lib/systemd/system/m2m-rpi.service && "
        'sudo systemctl daemon-reload && '
        'sudo systemctl enable m2m-rpi.service && '
        'sudo systemctl restart m2m-rpi.service')
    ssh.exec_command(cmd)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=(
            'Instaluje zdalnie na RPI via ssh.'
            'RPI musi mieć dostęp do internetu - zbyt wiele zależności'
            'RPI musi mieć aktywny serwer SSH'),
        add_help=False)
    parser.add_argument('-h', '--host', dest='host',
                        help='target RPI ', required=True)
    parser.add_argument('--port', dest='port', type=int,
                        default=22,
                        help='port used for ssh')
    parser.add_argument('-u', '--user', dest='user', required=True)
    parser.add_argument('-p', '--passwd', dest='password',
                        help='user password')
    parser.add_argument('--path', dest='path',
                        default='/opt/m2m',
                        help='remote path')
    parser.add_argument('--help', action='help')
    parser.add_argument('--ini', dest='ini', action='store_true',
                        help='include ini file')
    parser.add_argument('--db', dest='db', action='store_true',
                        help='include db file')
    parser.add_argument('--uninstall', dest='uninst', action='store_true',
                        help='delete files from RPI')

    args = parser.parse_args()
    if not args.password:
        args.password = getpass('Hasło dla RPI: ')

    tpath = '/tmp/m2m'
    lpath = '../rpi'
    rpath = '/opt/m2m'

    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.set_missing_host_key_policy(
        paramiko.AutoAddPolicy())
    ssh.connect(
        hostname=args.host,
        port=args.port,
        username=args.user,
        password=args.password)
    sftp = ssh.open_sftp()

    if args.uninst:
        uninstall()
    else:
        install()
        set_systemd()
