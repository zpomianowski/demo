# Quectel IoT Modem
Instalujemy `ppp` by wygodnie konfigurować modem i ustanawiać przez niego połączazenie sieciowe:
~~~~
:::bash
sudo apt install ppp
~~~~

Teraz konfigurujemy modem. Potem musimy poprawić wpis, w którym porcie USB mamy nasz modem (**ttyUSBX**):

~~~~
:::bash
sudo cp quectel* /etc/ppp/peer
~~~~

