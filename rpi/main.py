#! /usr/bin/env venv/bin/python2
# -*- coding: utf-8 -*-
import ConfigParser
import logging
from socket import gethostname
from logging.handlers import RotatingFileHandler
from time import sleep
from db_wizard import get_db

CFG = ConfigParser.ConfigParser(
    defaults=dict(
        influx='admin:admin@localhost:8086/m2m',
        concentratorName=gethostname()))
CFG.readfp(open('appconfig.ini', 'r'))
DB = get_db(CFG)
CNAME = CFG.get('service', 'concentratorName')
CFG.set('service', 'concentratorName', CNAME)
CFG.write(open('appconfig.ini', 'w'))


def get_logger():
    import getpass
    logfmt = "%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s"
    logging.basicConfig(level=CFG.getint('service', 'logLevel'), format=logfmt)
    logger = logging.getLogger()
    logfilepath = CFG.get('service', 'logPath')
    if 'm2m' not in getpass.getuser():
        logfilepath = logfilepath.replace(
            '.log', '-%s.log' % getpass.getuser())
    rothanlder = RotatingFileHandler(
        logfilepath, maxBytes=1024*1024, backupCount=9)
    rothanlder.setFormatter(logging.Formatter(logfmt))
    logger.addHandler(rothanlder)
    return logger


def _resetenv():
    import os
    from itertools import cycle
    from importer import Importer
    db.client.truncate()
    db.data.truncate()
    db.commit()

    imp = Importer(db)
    for fn in [fn for fn in os.listdir('data') if '.csv' in fn]:
        base, ext = os.path.splitext(fn)
        try:
            imp.parse(os.path.join('data', fn), base.replace('profile', ''))
        except:
            l.warn('Error parsing data from file "%s"' % fn)

    profiles = cycle(
        [r.profile for r in
         db(db.data.id > 0).select(db.data.profile, distinct=True)])
    for i in range(CFG.getint('service', 'measNb')):
        db.client.insert(
            clientid=str(i + 1).zfill(6),
            profile=next(profiles))
    db.commit()

if __name__ == '__main__':
    import argparse
    from data import DataFactory

    l = get_logger()
    parser = argparse.ArgumentParser(
        description='Symuluje koncentrator raportujący stan liczników.')
    parser.add_argument('-s', '--shell', dest='shell', action='store_true',
                        help='open the interactive shell')
    parser.add_argument('--reset', dest='reset', action='store_true',
                        help='reset the environment')
    parser.add_argument('--sim', dest='sim', nargs='?',
                        metavar='days to sim',
                        type=int,
                        const=CFG.getint('service', 'simDays'),
                        help='delete old data and run back in time simulation')
    parser.add_argument('--webdb', dest='webdb', action='store_true',
                        help='run web service to edit the database')

    args = parser.parse_args()

    interval = CFG.getfloat('service', 'interval')
    factory = DataFactory(DB, CFG, interval)

    db = DB
    if args.reset:
        _resetenv()
    elif args.shell:
        try:
            from IPython import embed
            embed(header=u"""
            Interactive shell:
            """)
        except ImportError:
            import code
            code.interact(local=globals())
    elif args.webdb:
        from subprocess import call
        call(['venv/bin/sqlite_web',
              'database/storage.db', '-H', '0.0.0.0', '-x'])
    else:
        if db(db.client.id > 0).count() == 0:
            _resetenv()
        factory.simulate(args.sim or CFG.getint('service', 'simDays'),
                         drop=args.sim is not None)
        sleep(interval)
        while True:
            factory.gen_samples()
            sleep(interval)