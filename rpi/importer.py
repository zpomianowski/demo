# -*- coding: utf-8 -*-
from csv import DictReader, Dialect
from datetime import datetime


class Importer(object):
    def __init__(self, db):
        self.db = db
        self.fn = ['date'] + ['h%02d' % i for i in range(1, 25)]

    def parse(self, filename='data/profileG.csv', profilename='G'):
        with open(filename, 'r') as csvfile:
            csv = DictReader(csvfile)
            for r in csv:
                r['d'] = datetime.strptime(
                    r['d'], '%Y-%m-%d').timetuple().tm_yday

                self.db.data.insert(
                    day=r['d'],
                    profile=profilename,
                    **{'h%02d' % int(k): v.replace(',', '.')
                       for (k, v) in r.items() if k != 'd'}
                )
            self.db.commit()