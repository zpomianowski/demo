# -*- coding: utf-8 -*-
import os
from socket import gethostname
from pydal import DAL, Field


if not os.path.exists('database'):
    os.mkdir('database')


def get_db(config):
    import sqlite3
    from json import loads, dumps
    db = DAL(config.get('db', 'connstr'), folder='database')

    db.define_table(
        'data',
        Field('profile', 'string', default='G'),
        Field('day', 'integer', default=1),
        *[Field('h%02d' % i, 'float', default=0) for i in range(1, 25)])


    db.define_table(
        'client',
        Field('source', 'string', default=gethostname),
        Field('clientid', 'string', default='0'.zfill(6)),
        Field('profile', 'string', default=None),
        Field('sample_energy', 'float', default=0),
        Field('sample_waterc', 'float', default=0),
        Field('sample_waterh', 'float', default=0),
        Field('sample_gas', 'float', default=0),
        Field('sum_energy', 'float', default=0),
        Field('sum_waterc', 'float', default=0),
        Field('sum_waterh', 'float', default=0),
        Field('sum_gas', 'float', default=0),
        Field('p_unimin', 'float', default=1.0),
        Field('p_unimax', 'float', default=1.05),
        Field('p_sigma', 'float', default=0.001),
        Field('p_ease', 'integer', default=1),
        Field('p_bezier_delay', 'string',
              default=[[0, 0], [0, 0], [1, 1], [1, 1]]),
        Field('p_bezier_suppr', 'string',
              default=[[0, 1], [0.5, 1], [0.5, 1], [1, 1]]))

    db.client.p_bezier_delay.filter_in = dumps
    db.client.p_bezier_delay.filter_out = loads
    db.client.p_bezier_suppr.filter_in = dumps
    db.client.p_bezier_suppr.filter_out = loads

    if 'sqlite' in config.get('db', 'connstr'):
        try:
            db.executesql(
                'CREATE INDEX IF NOT EXISTS data_profilex ON data (profile);'
                'CREATE INDEX IF NOT EXISTS data_dayx     ON data (day);')
        except sqlite3.Warning:
            pass

    return db
