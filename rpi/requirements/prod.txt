pyDAL==17.8
mysqlclient
influxdb==4.1.1
numpy==1.13.3
bezier==0.5.0
sqlite-web==0.1.8