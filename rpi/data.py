# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from random import random, gauss, uniform
from copy import deepcopy
from urlparse import urlparse
from influxdb import InfluxDBClient
from main import get_logger
from main import CNAME
from dateutil import tz, parser
import numpy as np
import bezier


logger = get_logger()
FZONE = tz.tzutc()
TZONE = tz.tzlocal()
TMPLT = dict(
    measurement="media",
    tags=dict(source=CNAME, clientid="0".zfill(6)),
    time=None,
    fields=dict(c=0, wh=0, wc=0, g=0,
                sc=0, swh=0, swc=0, sg=0)
)


def date_range(start, stop, sec_interval=3600):
    for n in xrange(int((stop - start).total_seconds() // sec_interval)):
        yield start + timedelta(seconds=n * sec_interval)

class Sample(object):
    def __init__(self, c=0, wh=0, wc=0, g=0):
        self.current = c
        self.water_hot = wh
        self.water_cold = wc
        self.gas = g


class DataFactory(object):
    def __init__(self, db, cfg, interval=3600):
        self.__db = db
        self.__cfg = cfg
        self.__cache = []
        uri = urlparse('//' + cfg.get('db', 'influx'))
        self._influx = InfluxDBClient(
            uri.hostname, uri.port, uri.username, uri.password,
            uri.path.replace('/', '').replace('\\', ''),
            timeout=50)
        self.__interval = interval

    def simulate(self, days=30, drop=True):
        import time
        start_time = time.time()
        db = self.__db

        stop_date = datetime.utcnow()
        cmd = 'SELECT time, c FROM media WHERE source=\'%s\'' % CNAME + \
              ' ORDER BY DESC LIMIT 1'
        logger.info(cmd)
        points = self._influx.query(cmd)
        if points:
            start_date = parser.parse(list(points)[0][0]['time'])
            start_date = start_date.replace(tzinfo=None) + \
                         timedelta(seconds=self.__interval)
            logger.info('Start date for sim %s' % start_date)
        if drop or not points:
            start_date = datetime.utcnow() - timedelta(days=days)
            self._influx.query(
                'DROP SERIES FROM media WHERE source=\'%s\'' % CNAME)
            db(db.client.id > 0).update(
                sum_energy=0, sum_waterc=0, sum_waterh=0, sum_gas=0)
            db.commit()

        for d in date_range(start_date, stop_date, self.__interval):
            logger.info('Gen samples for %s' % d)
            dur = timedelta(seconds=time.time() - start_time)
            logger.info('Simulation consumed %s [so far]' % dur)
            self.gen_samples(d, verbose=True)

        dur = timedelta(seconds=time.time() - start_time)
        logger.info('Simulation consumed %s [total]' % dur)

    def gen_samples(self, utc_dt=None, verbose=False):
        utc_dt = utc_dt if utc_dt else datetime.utcnow()
        fixfrac = 1. / (3600 // self.__interval)
        utc_stamp = utc_dt.isoformat() + 'Z'
        utc_dt = utc_dt.replace(tzinfo=FZONE)
        db = self.__db
        c = self.__cache

        def intersector(h, minv=0):
            return bezier.Curve(np.asfortranarray([
                [h + float(minv) / 1440, -1e7],
                [h + float(minv) / 1440, 1e7],
            ], dtype=np.float64), degree=2)

        for r in db(db.client.id > 0).iterselect():
            s = deepcopy(TMPLT)
            now = utc_dt.astimezone(TZONE).timetuple()
            curve_delay = bezier.Curve(
                np.asfortranarray(r.p_bezier_delay, dtype=np.float64),
                degree=len(r.p_bezier_delay)-1)
            curve_suppr = bezier.Curve(
                np.asfortranarray(r.p_bezier_suppr, dtype=np.float64),
                degree=len(r.p_bezier_delay)-1)
            tm_hour = now.tm_hour
            tm_min = float(now.tm_min)
            tm_hour_norm = float(tm_hour) / 24
            try:
                intercurve = intersector(tm_hour_norm, tm_min)
                tm_hour = min(int(intercurve.intersect(curve_delay)[0][1] * 24), 24)
            except:
                # TODO warning
                pass
            factor = 1.
            try:
                factor = intercurve.intersect(curve_suppr)[0][1]
            except:
                # TODO warning
                pass

            pdata = db((db.data.profile == r['profile']) &
                       (db.data.day == now.tm_yday)).select().first()

            v = gauss(uniform(
                r.p_unimin, r.p_unimax), r.p_sigma * fixfrac) * factor
            p_uni = (r.p_unimin + r.p_unimax) / 2
            v_bef = v * p_uni
            v_aft = v * p_uni
            if pdata:
                v = pdata['h%02d' % (
                    tm_hour if tm_hour != 0 else 24)] * fixfrac
                v = gauss(uniform(r.p_unimin * v, r.p_unimax * v),
                          r.p_sigma) * factor

                if r.p_ease > 0:
                    v_aft = []
                    v_bef = []
                    for x in range(1, r.p_ease + 1):
                        if now.tm_hour - x > 0:
                            v_bef.append(
                                pdata['h%02d' % (now.tm_hour - 1)
                                     ] * fixfrac * factor * p_uni)
                        if now.tm_hour + x < 24:
                            v_aft.append(
                                pdata['h%02d' % (now.tm_hour + 1)
                                     ] * fixfrac * factor * p_uni)

            if r.p_ease > 0 and pdata:
                w = [((60-tm_min)/60)*x for x in range(1, len(v_bef)+1)] + \
                    [r.p_ease + 2] + \
                    [(tm_min/60) * x for x in range(len(v_aft)+1, 1, -1)]
                r.sample_energy = \
                    sum([a*b for a, b in zip(w, v_bef + [v] + v_aft)]) / sum(w)
            else:
                r.sample_energy = v
            r.sample_waterc = float(random() * 15)
            r.sample_waterh = float(random() * 10)
            r.sample_gas = float(random() * 5)
            r.sum_energy += float(r.sample_energy)
            r.sum_waterh += float(r.sample_waterh)
            r.sum_waterc += float(r.sample_waterc)
            r.sum_gas += float(r.sample_gas)
            r.update_record()

            s['time'] = utc_stamp
            s['tags']['clientid'] = r.clientid
            s['fields'].update(
                c=r.sample_energy,
                wh=r.sample_waterh,
                wc=r.sample_waterc,
                g=r.sample_gas,
                sc=r.sum_energy,
                swh=r.sum_waterh,
                swc=r.sum_waterc,
                sg=r.sum_gas)
            c.append(s)
            if verbose:
                logger.debug('Meas %s: %f [kWh]' % (
                    r.clientid, r.sample_energy))


        db.commit()
        self._influx.write_points(self.serialize())

    def serialize(self):
        mlist = self.__cache
        jsondata = deepcopy(mlist)
        mlist = []
        return jsondata
